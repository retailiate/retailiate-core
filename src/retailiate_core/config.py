# -*- coding: utf-8 -*-
import os
from abc import ABCMeta, abstractmethod
from collections import MutableMapping
from pathlib import Path
from typing import Dict, Union


def getenv_boolean(var_name, default_value=False):
    result = default_value
    env_value = os.getenv(var_name)
    if env_value is not None:
        result = env_value.upper() in ("TRUE", "1")
    return result


# class RetailiateConfig(object):
#
#     def __init__(self,
#                  pulsar_host: str=None,
#                  pulsar_port: int=6650,
#                  minio_host: str=None,
#                  minio_access_key: str=None,
#                  minio_secret_key: str=None,
#                  minio_is_ssl: bool=True,
#                  postgres_db_name: str=None,
#                  postgres_host: str=None,
#                  postgres_port: int=5432,
#                  postgres_user: str=None,
#                  postgres_password: str=None,
#                  backend_cors_origins: List[str]=None
#                  ):
#
#         self._pulsar_host = pulsar_host
#         self._pulsar_port = pulsar_port
#
#         self._postgres_db_name = postgres_db_name
#         self._postgres_host = postgres_host
#         self._postgres_port = postgres_port
#         self._postgres_user = postgres_user
#         self._postgres_password = postgres_password
#         self._sqlalchemy_database_uri = f"postgresql://{self._postgres_user}:{self._postgres_password}@{self._postgres_host}:{self._postgres_port}/{self._postgres_db_name}"
#
#         self._minio_host = minio_host
#         self._minio_access_key = minio_access_key
#         self._minio_secret_key = minio_secret_key
#         self._minio_is_ssl = minio_is_ssl
#
#         self._backend_cors_origins = backend_cors_origins

NO_VALUE_MARKER = "__no_config_value__"
VALUE_UNSET_MARKER = "__value_unset__"
RAISE_ERROR_MARKER = "__raise_error__"


class ConfigResolver(metaclass=ABCMeta):
    @abstractmethod
    def get_config_value(self, config_key: str):
        pass


class EnvConfigResolver(ConfigResolver):
    def get_config_value(self, config_key: str):

        env_var = config_key.upper()
        value = os.getenv(env_var, NO_VALUE_MARKER)

        if value.lower() == "true":
            value = True
        elif value.lower() == "false":
            value = False
        else:
            try:
                value = int(value)
            except (Exception):
                pass

        return value


class DictConfigResolver(ConfigResolver):
    def __init__(self, data=None):
        if data is None:
            data = {}

        self._data = data

    def get_config_value(self, config_key: str):
        result = self._data.get(config_key, NO_VALUE_MARKER)
        return result


class FolderConfigResolver(ConfigResolver):
    def __init__(self, base_path: Union[str, Path]):

        if not isinstance(base_path, Path):
            base_path = Path(base_path)

        self._base_path: Path = base_path

    def get_config_value(self, config_key: str):

        file_path = self._base_path / config_key

        if not file_path.exists():
            return NO_VALUE_MARKER

        value = file_path.read_text()
        return value


class RetailiateConfig(MutableMapping):
    def __init__(self, *resolvers, defaults: Dict = None, guaranteed_keys=None):

        self._default_empty_key = None
        if guaranteed_keys is None:
            guaranteed_keys = []
        self._guaranteed_keys = guaranteed_keys

        self._custom_values = {}
        if not resolvers:
            resolvers = [EnvConfigResolver()]
        self._resolvers = [DictConfigResolver(self._custom_values)] + list(resolvers)

        if defaults:
            default_resolver = DictConfigResolver(data=defaults)
            self._resolvers.append(default_resolver)

        self._cache = {}

        for key in self._guaranteed_keys:
            getattr(self, key)

    def __getattr__(self, item):

        if item in self._cache.keys():
            return self._cache[item]

        result = NO_VALUE_MARKER
        for r in self._resolvers:
            value = r.get_config_value(item)
            if value == NO_VALUE_MARKER:
                continue
            result = value
            break

        if result == NO_VALUE_MARKER:
            if self._default_empty_key == RAISE_ERROR_MARKER:
                raise KeyError(f"No config value for key '{item}'.")
            result = self._default_empty_key

        if result == VALUE_UNSET_MARKER:
            if self._default_empty_key == RAISE_ERROR_MARKER:
                raise KeyError(f"Config value '{item}' is unset.")
            result = self._default_empty_key

        self._cache[item] = result
        return result

    def __getitem__(self, key):
        value = getattr(self, key)
        return value

    def __delitem__(self, key):

        self._custom_values[key] = VALUE_UNSET_MARKER

    def __setitem__(self, key, value):

        self._custom_values[key] = value

    def __iter__(self):
        return iter(self._cache)

    def __len__(self):
        return len(self._cache)

    def __repr__(self):

        return f"{type(self).__name__}({self._cache})"


# def from_env():
#
#     POSTGRES_HOST = os.getenv("POSTGRES_HOST")
#     POSTGRES_USER = os.getenv("POSTGRES_USER")
#     POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
#     POSTGRES_PORT = os.getenv("POSTGRES_PORT", 5432)
#     POSTGRES_DB = os.getenv("POSTGRES_DB")
#
#     PULSAR_HOST = os.getenv("PULSAR_HOST")
#     PULSAR_PORT = os.getenv("PULSAR_PORT", 6650)
#
#     MINIO_HOST = os.getenv("MINIO_HOST")
#     MINIO_ACCESS_KEY = os.getenv("MINIO_ACCESS_KEY")
#     MINIO_SECRET_KEY = os.getenv("MINIO_SECRET_KEY")
#     MINIO_IS_SSL = os.getenv("MINIO_IS_SSL", True)
#
#     config = RetailiateConfig(
#         pulsar_host=PULSAR_HOST,
#         pulsar_port=PULSAR_PORT,
#         minio_host=MINIO_HOST,
#         minio_access_key=MINIO_ACCESS_KEY,
#         minio_secret_key=MINIO_SECRET_KEY,
#         minio_is_ssl=MINIO_IS_SSL,
#         postgres_db_name=POSTGRES_DB,
#         postgres_host=POSTGRES_HOST,
#         postgres_port=POSTGRES_PORT,
#         postgres_user=POSTGRES_USER,
#         postgres_password=POSTGRES_PASSWORD
#     )
#     return config

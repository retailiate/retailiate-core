# -*- coding: utf-8 -*-
import os

import logging
from concurrent.futures.thread import ThreadPoolExecutor
from typing import Dict

from aiographql.client.client import GraphQLClient
from aiographql.client.transaction import GraphQLRequest
from botocore.config import Config
from minio import Minio
from minio.error import BucketAlreadyOwnedByYou, BucketAlreadyExists
from nats.aio.client import Client as NATS
from smart_open import open as so
from stan.aio.client import Client as STAN
from starlette.datastructures import UploadFile

from retailiate_core.config import (
    RetailiateConfig,
    FolderConfigResolver,
    EnvConfigResolver,
)

CONSUMER_LISTEN_POOL = ThreadPoolExecutor()

log = logging.getLogger("retailiate")


def consumer_receive(consumer, callback, *args, **kwargs):
    while True:
        msg = consumer.receive()
        try:
            callback(msg)
            consumer.acknowledge(msg)
        except (Exception) as e:
            consumer.negative_acknowledge(str(e))


class Retailiate(object):
    DEFAULT_CONFIG = {
        "nats_host": "nats.ikh",
        "nats_port": 4222,
        "stan_cluster_name": "stan",
        "minio_host": "minio-hl-svc.ikh",
        "minio_port": 9000,
        "minio_is_ssl": False,
        "postgres_db_name": "retailiate",
        "postgres_db_host": "retailiate-db-cluster.retailiate",
        "postgres_db_port": 5432,
        "postgres_db_user": "retailiate",
        "hasura_host": "hasura.retailiate",
        "hasura_port": 80,
        "hasura_is_ssl": False,
        "hasura_path": "v1/graphql",
    }
    GUARANTEED_KEYS = DEFAULT_CONFIG.keys()

    def __init__(self, defaults=DEFAULT_CONFIG, guaranteed_keys=GUARANTEED_KEYS):

        self._defaults = defaults
        self._guaranteed_keys = guaranteed_keys
        self._config = None
        self._nats_client = None
        self._stan_client = None
        self._minio_client = None
        self._s3_config = None
        self._hasura_client = None

        self._producers = {}

        # self.config

    @property
    def config(self) -> RetailiateConfig:

        if self._config is not None:
            return self._config
        ecr = EnvConfigResolver()
        fcr = FolderConfigResolver("/var/openfaas/secrets")
        self._config = RetailiateConfig(
            fcr, ecr, defaults=self._defaults, guaranteed_keys=self._guaranteed_keys
        )
        return self._config

    @property
    def s3_config(self):

        if self._s3_config is not None:
            return self._s3_config

        is_ssl = self.config.minio_is_ssl
        if isinstance(is_ssl, str):
            is_ssl = is_ssl.upper() in ("TRUE", "1", "YES")

        endpoint = f"{self.config.minio_host}:{self.config.minio_port}"

        self._s3_config = {}
        self._s3_config["host"] = self.config.minio_host
        self._s3_config["port"] = self.config.minio_port
        self._s3_config["endpoint"] = endpoint
        self._s3_config["protocol"] = "https" if is_ssl else "http"
        self._s3_config["is_ssl"] = is_ssl
        self._s3_config["access_key"] = self.config.minio_access_key
        self._s3_config["secret_key"] = self.config.minio_secret_key

        return self._s3_config

    @property
    def minio_client(self):

        if self._minio_client is not None:
            return self._minio_client

        config = self.s3_config
        self._minio_client = Minio(
            endpoint=config["endpoint"],
            access_key=config["access_key"],
            secret_key=config["secret_key"],
            secure=config["is_ssl"],
        )
        return self._minio_client

    def _create_resource_kwargs(self):

        config = self.s3_config
        resource_kwargs = {
            "endpoint_url": f"{config['protocol']}://{config['endpoint']}",
            "aws_access_key_id": config["access_key"],
            "aws_secret_access_key": config["secret_key"],
            "config": Config(signature_version="s3v4"),
        }

        return resource_kwargs

    def open_s3_object(self, bucket, object_name, mode="r"):

        resource_kwargs = self._create_resource_kwargs()
        obj = so(
            f"s3://{bucket}/{object_name}",
            mode=mode,
            transport_params={"resource_kwargs": resource_kwargs},
        )
        return obj

    @property
    def nats_client(self):

        if self._nats_client is not None:
            return self._nats_client

        self._nats_client = NATS()

        return self._nats_client

    async def nats_client_connected(self):

        nats_client = self.nats_client
        nats_host = self.config.nats_host
        nats_port = self.config.nats_port

        await self._nats_client.connect(f"{nats_host}:{nats_port}")
        return nats_client

    @property
    def stan_client(self):

        if self._stan_client is not None:
            return self._stan_client

        self._stan_client = STAN()

        return self._stan_client

    async def stan_client_connected(self, client_id):

        stan_cluster_name = self.config.stan_cluster_name
        stan_client = self.stan_client

        await stan_client.connect(
            cluster_id=stan_cluster_name,
            client_id=client_id,
            nats=await self.nats_client_connected(),
        )
        return stan_client

    @property
    def hasura_client(self):

        if self._hasura_client is not None:
            return self._hasura_client

        is_ssl = self.config.hasura_is_ssl
        if isinstance(is_ssl, str):
            is_ssl = is_ssl.upper() in ("TRUE", "1", "YES")

        proto = "https" if is_ssl else "http"
        self._hasura_client = GraphQLClient(
            endpoint=f"{proto}://{self.config.hasura_host}:{self.config.hasura_port}/{self.config.hasura_path}",
            # headers={"x-hasura-admin-secret": "myadminsecretkey"},
        )
        return self._hasura_client

    async def graphql_query(self, query: str, variables: Dict = None):

        if variables is None:
            variables = {}

        req = GraphQLRequest(
            query=query, variables=variables, operationName="InsertScannedItem"
        )

        result = await self.hasura_client.post(req)
        return result.response.data

    def download_object_to_file(self, bucket_name, object_name, file_path):

        self.minio_client.fget_object(
            bucket_name=bucket_name, object_name=object_name, file_path=file_path
        )

    def upload_to_bucket(
        self,
        bucket_name,
        object_name,
        upload_file,
        content_type="application/octet-stream",
    ):

        # Make a bucket with the make_bucket API call.
        try:
            self.minio_client.make_bucket(bucket_name=bucket_name)
        except BucketAlreadyOwnedByYou:
            pass
        except BucketAlreadyExists:
            pass
        # except ResponseError as err:
        #        raise

        if isinstance(upload_file, UploadFile):
            int_temp_file = upload_file.file
            int_temp_file.seek(0, 2)
            file_size = int_temp_file.tell()
            int_temp_file.seek(0)
        else:
            int_temp_file = None
            file_size = None
            print(type(upload_file))
            raise NotImplementedError()

        return self.minio_client.put_object(
            bucket_name,
            object_name,
            int_temp_file,
            file_size,
            content_type=content_type,
        )

    async def finalize(self):

        if self._stan_client is not None:
            await self.stan_client.close()
        if self._nats_client is not None:
            await self.nats_client.close()

    # def get_pulsar_producer(
    #     self,
    #     producer_name: str = None,
    #     topic: str = None,
    #     tenant: str = "public",
    #     namespace: str = "default",
    #     schema: Schema = None,
    #     create_subscriptions: Union[List[str], str, bool] = None,
    # ):
    #     """Create or retrieve existing producer.
    #
    #     Args:
    #         - *producer_name*: The producer name, required if 'topic' is not specified.. If not assigned,
    #              the system will generate a globally unique name which can be accessed
    #              with `Producer.producer_name()`. When specifying a name, it is app to
    #              the user to ensure that, for a given topic, the producer name is unique
    #              across all Pulsar's clusters.
    #         - *topic*: The topic to produce items for. If not specified, 'producer_name' is required.
    #         - *tenant*: The pulsar tenant name.
    #         - *namespace*: The pulsar namespace name.
    #         - *schema*: The schema for the produced messages. If empty, 'BytesSchema' will be used.
    #         - *create_subscriptions*: Try to auto-create (exclusive) subscriptions for the new topic with the specified
    #              name(s). If a boolean is provided, the topic name is used as subscription name. Only applies when 'topic'
    #              is provided. Errors will be ignored.
    #     """
    #
    #     if producer_name is not None:
    #         if producer_name in self._producers.keys():
    #             return self._producers[producer_name]
    #
    #     if topic is None:
    #         if producer_name:
    #             raise ValueError(
    #                 f"No producer with name '{producer_name}' created yet."
    #             )
    #         else:
    #             raise ValueError("No producer name or topic specified.")
    #
    #     topic_string = f"{tenant}/{namespace}/{topic}"
    #     if schema is None:
    #         schema = BytesSchema()
    #     elif not issubclass(schema.__class__, Schema):
    #         raise TypeError(
    #             f"Invalid type for schema, must be subclass of 'pulsar.schema.Schema': {type(schema)}"
    #         )
    #
    #     producer = self.pulsar_client.create_producer(
    #         topic_string, producer_name=producer_name, schema=schema
    #     )
    #
    #     if producer_name is not None:
    #         self._producers[producer_name] = producer
    #     else:
    #
    #         if create_subscriptions:
    #             if isinstance(create_subscriptions, str):
    #                 create_subscriptions = [create_subscriptions]
    #             elif isinstance(create_subscriptions, bool):
    #                 create_subscriptions = [topic]
    #
    #         for s in create_subscriptions:
    #             try:
    #                 c = self.pulsar_client.subscribe(
    #                     topic_string,
    #                     subscription_name=s,
    #                     consumer_type=ConsumerType.Exclusive,
    #                 )
    #                 c.close()
    #             except (Exception):
    #                 pass
    #
    #     return self._producers[producer_name]
    #
    # def create_pulsar_consumer(
    #     self,
    #     subscription_name,
    #     topic,
    #     callback,
    #     tenant="public",
    #     namespace="default",
    #     schema=None,
    # ):
    #
    #     if schema is None:
    #         schema = BytesSchema()
    #     consumer = self.pulsar_client.subscribe(
    #         f"{tenant}/{namespace}/{topic}",
    #         subscription_name=subscription_name,
    #         schema=schema,
    #     )
    #
    #     def wrapper(cons, cb):
    #
    #         while True:
    #             msg = cons.receive()
    #             try:
    #                 log.debug("Starting callback for event: {}".format(msg))
    #                 result = cb(msg)
    #                 log.debug(
    #                     "Callback result for {}: {}".format(subscription_name, result)
    #                 )
    #                 cons.acknowledge(msg)
    #             except (Exception) as e:
    #                 cons.negative_acknowledge(str(e))
    #
    #     future = CONSUMER_LISTEN_POOL.submit(wrapper, consumer, callback)
    #     return future


class Context:
    def __init__(self):
        self.hostname = os.environ.get("HOSTNAME", "localhost")

# -*- coding: utf-8 -*-
from pulsar.schema import Record, String


class JtlExtractEventSchema(Record):
    id = String()
    storage = String()
    bucket = String()
    object_name = String()

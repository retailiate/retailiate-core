# -*- coding: utf-8 -*-
import logging
from collections import Mapping, Sequence

import ujson
from starlette.responses import Response

log = logging.getLogger("retailiate")


def format_status_code(resp):
    if "statusCode" in resp:
        return resp["statusCode"]

    return 200


def format_body(resp):
    if "body" not in resp.keys():
        return ""
    elif isinstance(resp["body"], (Mapping, Sequence)):
        return ujson.dumps(resp["body"])
    else:
        return str(resp["body"])


def format_headers(resp):
    if "headers" not in resp.keys():
        return {}
    elif isinstance(resp["headers"], Sequence):
        headers = {}
        for header_tuple in resp["headers"]:
            if not isinstance(header_tuple, Sequence):
                raise TypeError(f"Invalid type for header tuple: {type(header_tuple)}")
            elif len(header_tuple) != 2:
                raise ValueError(f"Invalid header tuple: {header_tuple}")
            headers[header_tuple[0]] = header_tuple[1]
        return headers
    elif not isinstance(resp["headers"], Mapping):
        raise TypeError("Invalid type for 'headers'")

    return resp["headers"]


def format_response(resp):
    if resp is None:
        return Response("", 200)

    if isinstance(resp, Response) or issubclass(resp.__class__, Response):
        return resp

    if not isinstance(resp, Mapping) or (
        "body" not in resp.keys()
        and "statusCode" not in resp.keys()
        and "headers" not in resp.keys()
    ):
        resp = {"statusCode": 200, "body": resp}

    status_code = format_status_code(resp)
    body = format_body(resp)
    headers = format_headers(resp)

    return Response(body, status_code, headers)

# -*- coding: utf-8 -*-
import asyncio
import functools
from concurrent.futures import ThreadPoolExecutor

pool = ThreadPoolExecutor()


def to_async_method(fn):
    """
    turns a sync function to async function using threads
    """

    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        future = pool.submit(fn, *args, **kwargs)
        return asyncio.wrap_future(future)  # make it awaitable

    return wrapper


def to_sync_method(fn):
    """
    turn an async function to sync function
    """

    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        return to_sync(fn, *args, **kwargs)

    return wrapper


def to_sync(async_fn, *args, **kwargs):
    res = async_fn(*args, **kwargs)
    if asyncio.iscoroutine(res):
        return asyncio.get_event_loop().run_until_complete(res)
    return res


def click_coro(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        return to_sync(fn, *args, **kwargs)

    return wrapper

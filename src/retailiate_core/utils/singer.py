# -*- coding: utf-8 -*-
import json
import os
import tempfile
from abc import ABCMeta, abstractmethod
from typing import Dict

import click

from retailiate_core.core import Retailiate
from retailiate_core.utils.subprocesses import run_process


class WorkflowException(Exception):
    def __init__(self, *args):
        super(WorkflowException, self).__init__(*args)


class SingerTarget(metaclass=ABCMeta):
    @abstractmethod
    def target_config(self) -> Dict:
        pass

    @abstractmethod
    def target_command(self):
        pass


class PostgresSingerTarget(SingerTarget):
    def __init__(self, retailiate: Retailiate, schema: str, target_command=None):
        self._target_config = {
            "disable_collection": True,
            "postgres_database": retailiate.config.get("postgres_db_name"),
            "postgres_host": retailiate.config.get("postgres_db_host"),
            "postgres_password": retailiate.config.get("postgres_db_password"),
            "postgres_port": retailiate.config.get("postgres_db_port"),
            "postgres_schema": schema,
            "postgres_username": retailiate.config.get("postgres_db_user"),
        }
        if target_command is None:
            target_command = "/home/markus/.pyenv/versions/singer_target_postgres/bin/target-postgres"
            # target_command = "/opt/singer_target_postgres/bin/target-postgres"
        self._target_command = target_command

    def target_config(self):
        return self._target_config

    def target_command(self):
        return self._target_command


class SingerData(object):
    def __init__(self, retailiate: Retailiate, bucket: str, object_name: str):
        self._retailiate = retailiate
        self._bucket = bucket
        self._object_name = object_name

    @property
    def bucket(self):
        return self._bucket

    @property
    def object_name(self):
        return self._object_name


class SingerLoad(object):
    def __init__(self, retailiate: Retailiate, data: SingerData, target: SingerTarget):

        self._retailiate = retailiate
        self._data = data
        self._target = target

    @property
    def data(self):
        return self._data

    @property
    def target(self):
        return self._target

    def process(self):

        temp = None
        try:
            temp = tempfile.NamedTemporaryFile(mode="w", delete=False)
            config_path = temp.name
            json.dump(self._target.target_config(), temp)
            temp.close()

            input_obj = self._retailiate.open_s3_object(
                self._data.bucket, self._data.object_name
            )

            def stdout_cb(line):
                click.echo(line)

            def stderr_cb(line):
                click.echo(line, err=True)

            return_code = run_process(
                command=self._target.target_command(),
                args=("-c", config_path),
                stdin_obj=input_obj,
                stdout_callback=stdout_cb,
                stderr_callback=stderr_cb,
            )

            return return_code

        except (Exception) as e:
            import traceback

            traceback.print_exc()
            raise WorkflowException(e)
        finally:
            if temp is not None and os.path.exists(temp.name):
                # os.unlink(temp.name)
                print(temp.name)

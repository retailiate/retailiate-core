# -*- coding: utf-8 -*-
import logging

log = logging.getLogger("retailiate")


def run_flow(flow):
    log.debug("Starting")
    result_state = flow.run()

    log.debug("Finished")
    result = []

    for k, v in result_state.result.items():
        result.append({"name": k.name, "slug": k.slug, "result": v.result})

    resp = {"message": result_state.message, "result": result}

    return resp

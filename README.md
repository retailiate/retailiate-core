[![PyPI status](https://img.shields.io/pypi/status/retailiate-core.svg)](https://pypi.python.org/pypi/retailiate-core/)
[![PyPI version](https://img.shields.io/pypi/v/retailiate-core.svg)](https://pypi.python.org/pypi/retailiate-core/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/retailiate-core.svg)](https://pypi.python.org/pypi/retailiate-core/)
[![Pipeline status](https://gitlab.com/retailiate/retailiate-core/badges/develop/pipeline.svg)](https://gitlab.com/frkl/retailiate-core/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# retailiate-core

*Retailiate core components*


## Description

Core library for the 'retailiate' project. Contains helper utilities to create plugins/apps for a 'retailiate' instance.

Documentation: https://retailiate.gitlab.io/retailiate-core/

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'retailiate-core' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 retailiate_core
    git clone https://gitlab.com/frkl/retailiate_core
    cd <retailiate_core_dir>
    pyenv local retailiate_core
    pip install -e .[all-dev]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)

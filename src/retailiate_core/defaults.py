# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs

retailiate_core_app_dirs = AppDirs("retailiate_core", "frkl")

if not hasattr(sys, "frozen"):
    RETAILIATE_CORE_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `retailiate_core` module."""
else:
    RETAILIATE_CORE_MODULE_BASE_FOLDER = os.path.join(sys._MEIPASS, "retailiate_core")
    """Marker to indicate the base folder for the `retailiate_core` module."""

RETAILIATE_CORE_RESOURCES_FOLDER = os.path.join(
    RETAILIATE_CORE_MODULE_BASE_FOLDER, "resources"
)

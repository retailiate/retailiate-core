FROM debian:buster-slim

ENV LC_ALL C.UTF-8
ENV PATH="${PATH}:/root/.local/share/freckles/bin"

RUN apt-get update && \
    apt-get upgrade -y && \
      apt-get install --no-install-recommends -y curl git python3-apt python3-venv python3-setuptools python3-wheel python3-dev build-essential && \
      rm -rf /var/lib/apt/lists/* && \
      curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
      python3 get-pip.py && \
      curl https://freckles.sh | bash -s frecklecute debug-var ansible_env

COPY . /opt/retailiate_core
RUN pip3 install -r /opt/retailiate_core/requirements.txt && pip3 install --extra-index-url https://pkgs.frkl.io/frkl/dev /opt/retailiate_core/

ONBUILD COPY requirements.txt frecklets* /tmp/provision/

ONBUILD RUN cd /tmp/provision && \
      if [ -f "/tmp/provision/provision.frecklet" ]; then frecklecute -c callback=default::full -r /tmp/provision/ /tmp/provision/provision.frecklet; fi && \
      pip3 install -r requirements.txt && \
      rm -rf /root/.local/share/freckles && \
      rm -rf /var/lib/apt/lists/* && \
      rm -rf /root/.cache

CMD ["/usr/bin/bash"]

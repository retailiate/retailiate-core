# -*- coding: utf-8 -*-
import json

import asyncclick as click
from kubernetes_asyncio import config, client

from retailiate_core.core import Retailiate

click.anyio_backend = "asyncio"


@click.group()
@click.pass_context
async def cli(ctx):
    retailiate = Retailiate()
    ctx.obj = {}
    ctx.obj["retailiate"] = retailiate


@cli.command()
@click.pass_context
async def kubectl(ctx):

    # retailiate = ctx.obj["retailiate"]

    await config.load_kube_config()

    v1 = client.CoreV1Api()
    # print("Listing pods with their IPs:")
    ret = await v1.list_namespaced_pod(namespace="ikh-init")

    pw = None
    for i in ret.items:
        if "argocd-server" in i.metadata.name:
            pw = i.metadata.name
            break

    if not pw:
        raise Exception("No argocd-server pod")

    path = "api/v1/session"

    body = {"username": "admin", "password": pw}

    argo_cd_post_data = {
        "resource_path": "/api/v1/namespaces/{namespace}/services/{name}/proxy/{path}",
        "response_type": "str",
        "auth_settings": ["BearerToken"],
        "async_req": False,
        "_return_http_data_only": True,
    }

    get_bearer_token_data = {
        "method": "POST",
        "path_params": {
            "name": "https:argocd-server:443",
            "namespace": "ikh-init",
            "path": path,
        },
        "body": body,
        "header_params": {"Accept": "*/*"},
    }

    get_bearer_token_data.update(argo_cd_post_data)

    import pp

    pp(get_bearer_token_data)

    response = v1.api_client.call_api(**get_bearer_token_data)

    resp = await response
    resp = resp.replace("'", '"')
    token = json.loads(resp)["token"]

    print("TOKEN: {}".format(token))

    path = "api/v1/account/password"
    set_password_data = {
        "method": "PUT",
        "body": {"currentPassword": pw, "newPassword": "nixenixe25"},
        "path_params": {
            "name": "https:argocd-server:443",
            "namespace": "ikh-init",
            "path": path,
        },
        "header_params": {"Accept": "*/*", "Authorization": f"Bearer {token}"},
    }

    set_password_data.update(argo_cd_post_data)

    response = v1.api_client.call_api(**set_password_data)

    resp = await response
    resp = resp.replace("'", '"')
    print(resp)


# @cli.command()
# @click.pass_context
# async def consume(ctx):
#     retailiate = ctx.obj["retailiate"]
#     sc = await retailiate.stan_client_connected("client-123")
#
#     # Synchronous Publisher, does not return until an ack
#     # has been received from NATS Streaming.
#     await sc.publish("hi", b"hello")
#     await sc.publish("hi", b"world")
#
#     total_messages = 0
#     future = asyncio.Future()
#
#     async def cb(msg):
#         nonlocal future
#         nonlocal total_messages
#         print("Received a message (seq={}): {}".format(msg.seq, msg.data))
#         total_messages += 1
#         if total_messages >= 2:
#             future.set_result(None)
#
#     # Subscribe to get all messages since beginning.
#     sub = await sc.subscribe("hi", start_at="first", cb=cb)
#     await asyncio.wait_for(future, 1)
#
#     # Stop receiving messages
#     await sub.unsubscribe()
#
#     await retailiate.finalize()
#
#
# @cli.command()
# @click.pass_context
# def produce(ctx):
#     retailiate = ctx.obj["retailiate"]
#     nats_client = retailiate.nats_client_connected
#     print(nats_client)


if __name__ == "__main__":
    cli()

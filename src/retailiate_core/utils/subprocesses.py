# -*- coding: utf-8 -*-
import os
import subprocess
import time
from queue import Queue
from threading import Thread, Event
from typing import Tuple, Any, Callable

from plumbum import local


class PipeReader(Thread):
    def __init__(self, _fd):

        super(PipeReader, self).__init__()
        self._fd = _fd
        self._queue = Queue()
        self._stop_event = Event()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):

        for line in iter(self._fd.readline, None):
            if line:
                self._queue.put(line.decode("UTF-8"))
            else:
                if self.stopped():
                    break


def run_process(
    command: str,
    args: Tuple = (),
    stdin_obj: Any = None,
    stdout_callback: Callable = None,
    stderr_callback: Callable = None,
    timeout=None,
    poll_sleep_time=0.2,
):
    target_exe = local[command]

    process = target_exe.popen(
        args=args,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        preexec_fn=os.setsid,
        bufsize=1,
        close_fds=os.name == "posix",
    )

    stdout_reader = None
    if stdout_callback is not None:
        stdout_reader = PipeReader(process.stdout)
        stdout_reader.start()

    stderr_reader = None
    if stderr_callback is not None:
        stderr_reader = PipeReader(process.stderr)
        stderr_reader.start()

    input_thread = None
    if stdin_obj is not None:

        def pipe_input(stdin, inp_obj):
            for line in inp_obj:
                stdin.write(line.encode())

            stdin.close()

        input_thread = Thread(target=pipe_input, args=(process.stdin, stdin_obj))
        input_thread.daemon = True
        input_thread.start()

    if poll_sleep_time >= 0:

        while process.poll() is None:

            if stdout_reader is not None:
                if not stdout_reader._queue.empty():

                    if not stdout_reader._queue.empty():
                        line = stdout_reader._queue.get().strip()
                        if line:
                            stdout_callback(line)

            if stderr_reader is not None:
                if not stderr_reader._queue.empty():

                    if not stderr_reader._queue.empty():
                        line = stderr_reader._queue.get().strip()
                        if line:
                            stderr_callback(line)

            if stderr_reader._queue.empty() and stdout_reader._queue.empty():
                time.sleep(poll_sleep_time)

    if input_thread is not None:
        input_thread.join()

    process.wait(timeout=timeout)

    stdout_reader.stop()
    stderr_reader.stop()
    stdout_reader.join()
    stderr_reader.join()

    if stdout_reader is not None:
        while not stdout_reader._queue.empty():
            line = stdout_reader._queue.get().strip()
            if line:
                stdout_callback(line)

    if stderr_reader is not None:
        while not stderr_reader._queue.empty():
            line = stderr_reader._queue.get().strip()
            if line:
                stderr_callback(line)

    return process.returncode
